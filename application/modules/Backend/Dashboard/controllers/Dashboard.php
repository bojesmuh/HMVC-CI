<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		//$this->load->helper('url');
	}

	public function index()
	{
		$data = array(
			"title" => "Kinerja",
			"active" =>"active"
			);

		$this->load->view('template/header',$data);
		$this->load->view('v_dash',$data);	
		$this->load->view('template/footer',$data);	

	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/Back/Dashboard/controllers/Dashboard.php */