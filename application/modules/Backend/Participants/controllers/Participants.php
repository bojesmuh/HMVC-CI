<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data = array(
			'title' =>'Kinerja' ,
			"active" =>"active" );
		$this->load->view('template/header',$data);
		$this->load->view('v_list',$data);	
		$this->load->view('template/footer',$data);	
	}

	public function add()
	{
		# code...

		$data = array(
			'title' =>'Kinerja' , );
		$this->load->view('template/header',$data);
		$this->load->view('form_add',$data);	
		$this->load->view('template/footer',$data);	
	}

}

/* End of file Feedback.php */
/* Location: ./application/modules/Backend/Feedback/controllers/Feedback.php */