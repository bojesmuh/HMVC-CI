<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data = array(
			"title" => "Kinerja",
			"active" =>"active"
			);

		$this->load->view('template/header',$data);
		$this->load->view('v_setting',$data);	
		$this->load->view('template/footer',$data);	
		
	}

}

/* End of file Setting.php */
/* Location: ./application/modules/Backend/Setting/controllers/Setting.php */