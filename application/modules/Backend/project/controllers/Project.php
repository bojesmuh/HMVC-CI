<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model('M_project','mp');
	}

	public function index()
	{
		$data = array(
			'title' =>'My Project | Kinerja' ,
			'action' => site_url('project/add_action')); // variabel action di from untuk simpan );
		$this->load->view('template/nav', $data, FALSE);
		$this->load->view('v_list', $data, FALSE);
		$this->load->view('template/footer', $data, FALSE);
	}

	public function add_action()
	{
		$this->_rules(); //  function data form validation

        if ($this->form_validation->run() == FALSE) { // untuk kondisi jika from tidak di isi dan di isi
            $this->index(); // jika from tidak di isi maka ada notifikasi untuk di isi.
        } else {

            $data = array(
        		'Titletbl_project' => $this->input->post('title_project',TRUE),
        		'Datetbl_project' => date('Y-m-d h:i:s'),
                //'status' => $this->input->post('status',TRUE),
    	    );

            $insert = $this->mp->save($data);
            if ($insert > 0) {
            	# code...
            $this->session->set_flashdata('message_text', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Create Record Success</div>');
            redirect(site_url('project'));
            } else {
            	$this->session->set_flashdata('message_text', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Create Record Not Success</div>');
            redirect(site_url('project'));
            }
        }
		
	}

	public function ajax_list()
	{
		$list = $this->mp->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = "
			<a class=\"\" href='".base_url($r->idtbl_project)."/participants'>".$r->Titletbl_project."</a>";
			// $row[] = $r->Titletbl_project;
			$row[] = $r->IDtbl_member;
			$row[] = $r->IDtbl_member;
			$row[] = $r->Nametbl_member;

			

			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mp->count_all(),
						"recordsFiltered" => $this->mp->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function _rules() 
    {
		$this->form_validation->set_rules('title_project', ' ', 'trim|required');

		$this->form_validation->set_rules('idtbl_project', 'idtbl_project', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Project.php */
/* Location: ./application/modules/Backend/project/controllers/Project.php */