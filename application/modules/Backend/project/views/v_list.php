<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                    <div class="space-action">
                        <a data-toggle="modal" href='#modal-id' class="btn btn-info btn-fill"> Add Project </a>
                         <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                    </div>
                     <?php echo $this->session->flashdata('message_text'); ?>
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Collect Project</h4>
                                
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Project Title</th>
                                    	<th>Status</th>
                                    	<th>Num. people assessed</th>
                                    	<th>Owner</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- modal -->
        <div class="modal fade" id="modal-id">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">From Project</h4>
                    </div>
                    <div class="modal-body">

                     <div class="content">
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Enter Title Project</label>
                                                <input type="text" name="title_project" class="form-control" placeholder="Enter Title Project" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-fill">Save</button>
                                     <button type="button" class="btn btn-danger btn-fill" data-dismiss="modal">Close</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

    var save_method; //for save method string
    var table;
    // $(document).ready(function() {
      table = $('.table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('project/ajax_list'); ?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
    

  </script>



